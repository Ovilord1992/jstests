const color = ["#C92020","#2C1AB5","#26A821"];
const enumIdSqiare = ["square1","square2","square3"];
var randomOnload = [];
var finalValue = [];
var set = new Set();
var enumerated = 0;

window.onload = function() {
	listRandom();
}

function setId(getId) {
	if(getId.id == enumIdSqiare[0]){
		enumerated = 1; 
	}else if (getId.id == enumIdSqiare[1]){
		enumerated = 2; 
	}else{
		enumerated = 3; 
	}
	listRandom();
}

function listRandom(){
	while(color.length != set.size){
		var randomNum = Math.round(Math.random() * 2);
		set.add(randomNum);
	}
	elementСonversion();
}

function elementСonversion(){
	for(let value of set){
		randomOnload.push(value);
		if(value == 0){
			finalValue.unshift(value);
		}
		else{
			finalValue.push(value);
		}
		
	}
	colorAssignment();
}

function colorAssignment(){
	let colo1 = randomOnload[0];
	let colo2 = randomOnload[1];
	let colo3 = randomOnload[2];

	let color1 = finalValue[0];
	let color2 = finalValue[1];
	let color3 = finalValue[2];

	switch (enumerated) {
		case 1:
			document.querySelector("#square1").style.background = color[color1];
			document.querySelector("#square2").style.background = color[color2];
			document.querySelector("#square3").style.background = color[color3];
		  break;
		case 2:
			document.querySelector("#square1").style.background = color[color2];
			document.querySelector("#square2").style.background = color[color1];
			document.querySelector("#square3").style.background = color[color3];
		  break;
		case 3:
			document.querySelector("#square1").style.background = color[color2];
			document.querySelector("#square2").style.background = color[color3];
			document.querySelector("#square3").style.background = color[color1];
		  break;
		default:
			document.querySelector("#square1").style.background = color[colo1];
			document.querySelector("#square2").style.background = color[colo2];
			document.querySelector("#square3").style.background = color[colo3];
	  }
	randomOnload = [];
	finalValue=[];
	set.clear();
}
function outKey(key){
    var p1;
    p1 = document.getElementById('paragraphBottam1')
    p1.innerHTML = key;
}

function outNum(number){
    var p2;
    var p21; 
    p2 = document.getElementById('paragraphBottam2')
    p2.innerHTML = number;
    p21 = document.getElementById('paragraphNumberKey')
    p21.innerHTML = number;

}

function outCode(code){
    var p3;
    p3 = document.getElementById('paragraphBottam3')
    p3.innerHTML = code;
}

document.onkeydown = function (event){                    
    if (event.key){ 
        outKey(event.key);
    }

    if (event.which){ 
        outNum(event.which);
    }

    if (event.code){ 
        outCode(event.code);
    }
}
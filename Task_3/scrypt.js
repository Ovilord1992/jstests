let SlassSquare;
let rand;
const QUANTITY_SQUARE = 3;
const RIGTH_SQUARE = 3;
const LEFT_SQUARE = 0;

let randomSquare = function(){
    SlassSquare = document.getElementsByClassName("square");
    rand = Math.round(Math.random() * QUANTITY_SQUARE);          
}
document.onkeydown = function (event){                    
    if (event.code == "ArrowRight"){  
        defaultColor();
        if (rand  != RIGTH_SQUARE){
            rand +=1;
        }else{
            rand = LEFT_SQUARE;
        }                         
        randColor();
    }
    if (event.code == "ArrowLeft"){ 
        defaultColor();
        if (rand  != LEFT_SQUARE){                          
        rand -=1;
        }else{
            rand = RIGTH_SQUARE;
        } 
        randColor();
    } 
}

function randColor(){
    SlassSquare[rand].style.backgroundColor = "yellow";        
    SlassSquare[rand].style.outline = "5px dashed #B40431";  
}

function defaultColor(){
    SlassSquare[rand].style.backgroundColor = "blue";         
    SlassSquare[rand].style.outline = "none";    
}

window.onload = function() { 
    randomSquare();                                             
    randColor();
}
const color = [
  "#DF0101",
  "#FFFF00",
  "#00FF00",
  "#2E2EFE",
  "#B404AE",
  "#BDBDBD"
];

let SQUARES_COUNT = 3;
let ACTIVE_SQUARE_ID = null;

function setActive(squareId) {
  squareId = squareId > SQUARES_COUNT ? 1 : squareId;
  squareId = squareId < 1 ? SQUARES_COUNT : squareId;

  let z = document.getElementById("square" + squareId);
  z.addEventListener("click", function() {
    active(this);
  });
  ACTIVE_SQUARE_ID = squareId;
}

//перемешивание массива
function shuffle(arr) {
  var j, temp;
  for (var i = arr.length - 1; i > 0; i--) {
    j = Math.floor(Math.random() * (i + 1));
    temp = arr[j];
    arr[j] = arr[i];
    arr[i] = temp;
  }
  return arr;
}

//определение границы экрана, ТРЕБУЕТСЯ УПРОЩЕНИЕ
function dragendDrop(squareDragend) {
  squareDragend.draggable = "true";
  squareDragend.addEventListener("dragend", function(evel) {
    if (evel.pageY < 0 + squareDragend.offsetWidth / 2) {
      squareDragend.style.top = 0 + "px";
    } else if (
      evel.pageY >
      Math.max(window.innerHeight) - squareDragend.offsetWidth / 2
    ) {
      squareDragend.style.top =
        Math.max(window.innerHeight) - squareDragend.offsetWidth + "px";
    } else {
      squareDragend.style.top =
        evel.pageY - squareDragend.offsetWidth / 2 + "px";
    }
    if (evel.pageX < 0 + squareDragend.offsetWidth / 2) {
      squareDragend.style.left = 0 + "px";
    } else if (
      evel.pageX >
      Math.max(window.innerWidth) - squareDragend.offsetWidth / 2
    ) {
      squareDragend.style.left =
        Math.max(window.innerWidth) - squareDragend.offsetWidth + "px";
    } else {
      squareDragend.style.left =
        evel.pageX - squareDragend.offsetWidth / 2 + "px";
    }
  });
}

//двигаем активный блок
function moveTop(squareId, direction, gg) {
  let el = document.getElementById("square" + squareId).getBoundingClientRect();
  let pos = gg == "top" ? true : false;
  let currentTop = pos ? el.top : el.left;

  currentTop = Math.min(
    Math.max(0, currentTop + direction),
    pos ? window.innerHeight - el.height : window.innerWidth - el.width
  );
  pos
    ? (document.getElementById("square" + squareId).style.top =
        currentTop + "px")
    : (document.getElementById("square" + squareId).style.left =
        currentTop + "px");
}

//задаем цвет
function setColorRandom(el) {
  let z = (el.style.background = shuffle(color).pop());
  color.push(z);
}

function active(el) {
  setColorRandom(el)
  dragendDrop(el);
  return el;
}


//рандомная позиция элемента при загрузке, "отнимаем велечину квадрата для рандомизации в пределах окна"
function randomPositionOnload(el) {
  let wid = window.innerWidth - el.getBoundingClientRect().width;
  let heig = window.innerHeight - el.getBoundingClientRect().height;
  el.style.top = Math.round(Math.random() * heig) + "px";
  el.style.left = Math.round(Math.random() * wid) + "px";
}

//создаем родительский контейнер
function createWrap(){
  let wrap = document.createElement("div");
  wrap.id = "wrap";
  document.body.appendChild(wrap);
}

function setRandomPosAndColor(pos) {
    randomPositionOnload(pos);
    pos.style.background = shuffle(color).pop();
}

function createElementInWrap() {
  let container = document.getElementById("wrap");
  for (let i = 1; i <= SQUARES_COUNT; i++) {
    let div = document.createElement("div");
    div.classList.add("square");
    div.id = "square" + i;
    div.onclick = setActive.bind(this, i);
    container.appendChild(div);
    setRandomPosAndColor(div);
  }

}

let zzz = document.querySelectorAll("div");

function MacroCollision(obj1,obj2){
  var XColl=false;
  var YColl=false;

  if ((obj1.x + obj1.width >= obj2.x) && (obj1.x <= obj2.x + obj2.width)) XColl = true;
  if ((obj1.y + obj1.height >= obj2.y) && (obj1.y <= obj2.y + obj2.height)) YColl = true;

  if (XColl&YColl){return true;}
  return false;
}

window.onload = function() {
  createWrap();
  createElementInWrap();
  
  document.onkeydown = function(event) {
    // if (event.code == "KeyA") {
    //   setColorRandom();
    // }
    if (event.code == "ArrowUp") {
      return moveTop(ACTIVE_SQUARE_ID, -20, "top");
    }
    if (event.code === "ArrowDown") {
      return moveTop(ACTIVE_SQUARE_ID, 20, "top");
    }

    if (event.code == "ArrowRight") {
      return moveTop(ACTIVE_SQUARE_ID, 20, "left");
    }
    if (event.code == "ArrowLeft") {
      return moveTop(ACTIVE_SQUARE_ID, -20, "left");
    }
  };
};
